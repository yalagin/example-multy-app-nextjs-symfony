<?php

namespace App\Repository;

use App\Entity\PersonTag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PersonTag>
 *
 * @method PersonTag|null find($id, $lockMode = null, $lockVersion = null)
 * @method PersonTag|null findOneBy(array $criteria, array $orderBy = null)
 * @method PersonTag[]    findAll()
 * @method PersonTag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonTagsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonTag::class);
    }

//    /**
//     * @return PersonTags[] Returns an array of PersonTags objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?PersonTags
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
