<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\MediaObjectTagsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MediaObjectTagsRepository::class)]
#[ApiResource]
class MediaObjectTag
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiFilter(SearchFilter::class, strategy: "exact")]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToMany(targetEntity: MediaObject::class, inversedBy: 'mediaObjectTags')]
    private Collection $medaObject;

    public function __construct()
    {
        $this->medaObject = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, MediaObject>
     */
    public function getMedaObject(): Collection
    {
        return $this->medaObject;
    }

    public function addMedaObject(MediaObject $medaObject): static
    {
        if (!$this->medaObject->contains($medaObject)) {
            $this->medaObject->add($medaObject);
        }

        return $this;
    }

    public function removeMedaObject(MediaObject $medaObject): static
    {
        $this->medaObject->removeElement($medaObject);

        return $this;
    }
}
