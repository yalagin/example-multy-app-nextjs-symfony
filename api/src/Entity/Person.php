<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Repository\PersonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: PersonRepository::class)]
#[ApiResource]
class Person
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiFilter(SearchFilter::class, strategy: "exact")]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotNull]
    #[ApiFilter(SearchFilter::class, strategy: 'ipartial')]
    private ?string $fullName = null;

    #[ORM\Column(length: 255)]
    #[Assert\NotNull]
    #[ApiFilter(SearchFilter::class, strategy: 'ipartial')]
    private ?string $country = null;

    #[ORM\Column(type: Types::SMALLINT)]
    #[Assert\NotNull]
    #[ApiFilter(SearchFilter::class, strategy: 'exact')]
    private ?int $gender = null;

    #[ORM\Column(nullable: true)]
    #[ApiFilter(BooleanFilter::class)]
    private ?bool $isLocal = null;

    #[ORM\OneToMany(mappedBy: 'person', targetEntity: MediaObject::class)]
    private Collection $recording;

    #[ORM\ManyToMany(targetEntity: PersonTag::class, mappedBy: 'person')]
    private Collection $personTags;

    #[ORM\ManyToOne(inversedBy: 'person')]
    private ?Age $age = null;



    public function __construct()
    {
        $this->recording = new ArrayCollection();
        $this->personTags = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGender(): ?int
    {
        return $this->gender;
    }

    public function setGender(int $gender): static
    {
        $this->gender = $gender;

        return $this;
    }

    public function isIsLocal(): ?bool
    {
        return $this->isLocal;
    }

    public function setIsLocal(?bool $isLocal): static
    {
        $this->isLocal = $isLocal;

        return $this;
    }

    /**
     * @return Collection<int, MediaObject>
     */
    public function getRecording(): Collection
    {
        return $this->recording;
    }

    public function addRecording(MediaObject $recording): static
    {
        if (!$this->recording->contains($recording)) {
            $this->recording->add($recording);
            $recording->setPerson($this);
        }

        return $this;
    }

    public function removeRecording(MediaObject $recording): static
    {
        if ($this->recording->removeElement($recording)) {
            // set the owning side to null (unless already changed)
            if ($recording->getPerson() === $this) {
                $recording->setPerson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PersonTag>
     */
    public function getPersonTags(): Collection
    {
        return $this->personTags;
    }

    public function addPersonTag(PersonTag $personTag): static
    {
        if (!$this->personTags->contains($personTag)) {
            $this->personTags->add($personTag);
            $personTag->addPerson($this);
        }

        return $this;
    }

    public function removePersonTag(PersonTag $personTag): static
    {
        if ($this->personTags->removeElement($personTag)) {
            $personTag->removePerson($this);
        }

        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): static
    {
        $this->fullName = $fullName;

        return $this;
    }
    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }

    public function getAge(): ?Age
    {
        return $this->age;
    }

    public function setAge(?Age $age): static
    {
        $this->age = $age;

        return $this;
    }

}
