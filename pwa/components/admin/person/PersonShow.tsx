import {FieldGuesser, ShowGuesser, type ShowGuesserProps} from "@api-platform/admin";
import {ChipField, ReferenceArrayField, ReferenceField, SelectField, SingleFieldList, TextField} from "react-admin";
import countryList from "react-select-country-list";
import AudioField from "@/components/admin/media/MediaField";
import React from "react";

const PersonShow = (props:ShowGuesserProps) => (
  <ShowGuesser {...props}>
    <TextField  source={"id"}/>
    <TextField  source={"fullName"}/>
    <ReferenceField label="age" reference="ages" source="age" link="show">
      <ChipField source="name"  />
    </ReferenceField>
    <SelectField source="country" choices={countryList().getData()}
                 optionText="label"
                 optionValue="value"
                 name={"country"}/>
    <SelectField source="gender" choices={[
      { id: 0, name: 'Male' },
      { id: 1, name: 'Female' },
    ]}/>
    <FieldGuesser source={"isLocal"}/>
    <ReferenceArrayField label="Recordings" reference="media_objects" source="recording">
      <SingleFieldList>
        <AudioField  source={"contentUrl"} />
      </SingleFieldList>
    </ReferenceArrayField>
    <ReferenceArrayField  label="Person Tags" reference="person_tags" source="personTags">
      <SingleFieldList>
        <ChipField source="name" link="show" />
      </SingleFieldList>
    </ReferenceArrayField>
  </ShowGuesser>
);
export default PersonShow;
