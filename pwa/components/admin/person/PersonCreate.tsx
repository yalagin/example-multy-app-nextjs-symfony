import {CreateGuesser, InputGuesser} from "@api-platform/admin";
import {CreateGuesserProps} from "@api-platform/admin/src/types";
import {
  AutocompleteArrayInput, AutocompleteInput,
  ReferenceArrayInput,
  ReferenceInput,
  SelectField,
  SelectInput,
  TextField,
  TextInput
} from "react-admin";
import React, {useMemo} from "react";
import countryList from "react-select-country-list";
import {Form} from "@/components/admin/person/Form";

const PersonCreate = (props:CreateGuesserProps) => {
  return (
    <CreateGuesser {...props}>
      <Form/>

    </CreateGuesser>
  );
};

export default PersonCreate;
