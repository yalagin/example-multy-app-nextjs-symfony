import {
  AutocompleteArrayInput,
  AutocompleteInput,
  ReferenceArrayInput,
  ReferenceInput,
  required,
  SelectInput,
  TextInput
} from "react-admin";

import { ConditionInput } from "@/components/admin/book/ConditionInput";
import { BookInput } from "@/components/admin/book/BookInput";
import countryList from "react-select-country-list";
import {InputGuesser} from "@api-platform/admin";
import React from "react";

export const Form = () => (
  <>
    <TextInput validate={required()} source={"Full Name"} name={"fullName"}/>
    <ReferenceInput   source="age" reference="ages">
      <AutocompleteInput label="Person age" optionText="name" optionValue="@id"/>
    </ReferenceInput>
    <SelectInput source="country" choices={countryList().getData()}
                 optionText="label"
                 optionValue="value"
                 name={"country"}/>
    <SelectInput validate={required()} source="gender" defaultValue={0} choices={[
      { id: 0, name: 'Male' },
      { id: 1, name: 'Female' },
    ]} />
    <InputGuesser source={"isLocal"}/>
    <ReferenceArrayInput source="personTags" reference="person_tags">
      <AutocompleteArrayInput label="attributes" optionText="name" optionValue="@id"/>
    </ReferenceArrayInput>
    {/*<InputGuesser source={"recording"} label={"in future you will "}/>*/}
  </>
);
