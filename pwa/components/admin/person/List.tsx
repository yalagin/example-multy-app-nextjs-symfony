import {FieldGuesser, ListGuesser, type ListGuesserProps} from "@api-platform/admin";
import {
  ReferenceField,
  ReferenceArrayField,
  TextField,
  SingleFieldList,
  ChipField,
  SelectField,
} from "react-admin";
import countryList from "react-select-country-list";
import React from "react";
import AudioField from "@/components/admin/media/MediaField";

const PersonList = (props: ListGuesserProps) => (
  <ListGuesser {...props}>
    <TextField  source={"fullName"}/>
    <ReferenceField label="age" reference="ages" source="age" link="show">
        <ChipField source="name"  />
    </ReferenceField>
    <SelectField source="country" choices={countryList().getData()}
                 optionText="label"
                 optionValue="value"
                 name={"country"}/>
    <SelectField source="gender" choices={[
      { id: 0, name: 'Male' },
      { id: 1, name: 'Female' },
    ]}/>
    <FieldGuesser source={"isLocal"}/>
    <ReferenceArrayField label="Recordings" reference="media_objects" source="recording">
      <SingleFieldList>
        <AudioField  source={"contentUrl"} />
      </SingleFieldList>
    </ReferenceArrayField>
    <ReferenceArrayField label="Person Tags" reference="person_tags" source="personTags">
      <SingleFieldList>
        <ChipField source="name" />
      </SingleFieldList>
    </ReferenceArrayField>
  </ListGuesser>
);

export default PersonList;
