import {EditGuesser, EditGuesserProps} from "@api-platform/admin";
import {Form} from "@/components/admin/person/Form";


// @ts-ignore

export const PersonEdit = (props: EditGuesserProps) => (
  // @ts-ignore
  <EditGuesser {...props} title="Edit Person">
    <Form/>
  </EditGuesser>
);
