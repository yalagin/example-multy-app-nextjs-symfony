import {EditGuesser, EditGuesserProps} from "@api-platform/admin";
import {Form} from "@/components/admin/person/Form";
import {MediaForm} from "@/components/admin/media/Form";


// @ts-ignore

export const MediaEdit = (props: EditGuesserProps) => (
  <EditGuesser {...props} title="Edit Media">
    <MediaForm/>
  </EditGuesser>
);
