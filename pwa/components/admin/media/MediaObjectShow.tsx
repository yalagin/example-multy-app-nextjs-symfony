import {FieldGuesser, ShowGuesser,type ShowGuesserProps} from "@api-platform/admin";
import AudioField from "@/components/admin/media/MediaField";
import {ChipField, ReferenceArrayField, ReferenceField, SingleFieldList} from "react-admin";
import React from "react";

 const MediaObjectShow = (props:ShowGuesserProps)  => (
  <ShowGuesser {...props}>
    <AudioField  source={"contentUrl"} />
    <FieldGuesser source={"people"} />
    <ReferenceField label="Person" reference="people" source="person" >
      <SingleFieldList linkType="show">
      <ChipField source="fullName"  />
      </SingleFieldList>
    </ReferenceField>
    <ReferenceArrayField label="Audio Tags" reference="media_object_tags" source="mediaObjectTags">
      <SingleFieldList linkType="show">
        <ChipField source="name" />
      </SingleFieldList>
    </ReferenceArrayField>
  </ShowGuesser>
);


export default MediaObjectShow;
