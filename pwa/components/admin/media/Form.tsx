import {AutocompleteArrayInput, AutocompleteInput, ReferenceArrayInput, ReferenceInput} from "react-admin";
import React from "react";
import {FileField, FileInput} from "ra-ui-materialui";

export const MediaForm = () => (
  <>
    <ReferenceInput source="person" reference="people" name={"person"}>
      <AutocompleteInput
        filterToQuery={searchText => ({fullName: searchText})}
        optionText="fullName"
        label="person"
      />
    </ReferenceInput>
    <ReferenceArrayInput source="mediaObjectTags" reference="media_object_tags">
      <AutocompleteArrayInput label="attributes" optionText="name" optionValue="@id"/>
    </ReferenceArrayInput>
    <FileInput source="file" name={"file"} accept="audio/mp3,audio/*">
      <FileField source="src" title="title"/>
    </FileInput>
  </>
);
