import {CreateGuesser, InputGuesser, ListGuesserProps} from "@api-platform/admin";
import {CreateGuesserProps} from "@api-platform/admin/src/types";
import {FileField, FileInput} from "ra-ui-materialui";
import React from "react";
import {AutocompleteInput, ReferenceInput} from "react-admin";
import {MediaForm} from "@/components/admin/media/Form";

const MediaObjectsCreate = (props: CreateGuesserProps) => (
  <CreateGuesser {...props}>
    <MediaForm/>
  </CreateGuesser>
);

export default MediaObjectsCreate;
