import { useRecordContext } from 'react-admin';

// @ts-ignore
const AudioField = ({ source }) => {
  const record = useRecordContext();
  return (<audio src={record && record[source]} controls>Your browser does not support the audio element.</audio>);
};
export default AudioField;
