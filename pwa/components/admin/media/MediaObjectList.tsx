import {FieldGuesser, ListGuesser, ListGuesserProps} from "@api-platform/admin";
import AudioField from "@/components/admin/media/MediaField";
import {ChipField, NumberField, ReferenceArrayField, ReferenceField, SingleFieldList} from "react-admin";
import React from "react";

const MediaObjectList =  (props: ListGuesserProps) => (
  <ListGuesser {...props}>
    <NumberField source={"id"} />
    <AudioField  source={"contentUrl"} />
    <ReferenceField label="Person" reference="people" source="person" >
        <ChipField source="fullName"  />
    </ReferenceField>
    <ReferenceArrayField label="Audio Tags" reference="media_object_tags" source="mediaObjectTags">
      <SingleFieldList linkType="show">
        <ChipField source="name" />
      </SingleFieldList>
    </ReferenceArrayField>
  </ListGuesser>
);

export default MediaObjectList;
