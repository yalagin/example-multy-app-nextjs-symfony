import {FieldGuesser, ListGuesser} from "@api-platform/admin";
import {ChipField, ReferenceArrayField, SingleFieldList, TextField} from "react-admin";
import AuthorField from "@/components/admin/customFields/AuthorField";

export  const PersonTagList = (props:any) => (
    <ListGuesser {...props}>
      <FieldGuesser source={"name"} />
      <ReferenceArrayField label="Person" reference="people" source="person" >
        <SingleFieldList linkType="show" >
          <ChipField source="fullName"  />
        </SingleFieldList>
      </ReferenceArrayField>
    </ListGuesser>
  );
