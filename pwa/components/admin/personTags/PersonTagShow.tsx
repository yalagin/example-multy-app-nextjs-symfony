import {FieldGuesser, ShowGuesser} from "@api-platform/admin";
import {ChipField, ReferenceArrayField, SingleFieldList} from "react-admin";

const PersonTagShow = (props:any) => (
  <ShowGuesser {...props}>
    <FieldGuesser source={"id"} />
    <FieldGuesser source={"name"} />
    <ReferenceArrayField label="Person" reference="people" source="person" >
      <SingleFieldList linkType="show">
        <ChipField source="fullName"/>
      </SingleFieldList>
    </ReferenceArrayField>
  </ShowGuesser>
);
export default PersonTagShow;
