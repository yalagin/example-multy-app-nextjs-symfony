import {BookInput} from "@/components/admin/book/BookInput";
import {ReferenceArrayInput, required, SelectArrayInput, TextInput} from "react-admin";
import {ConditionInput} from "@/components/admin/book/ConditionInput";
import React from "react";

export const PersonTagsForm = () =>  (
  <>
    <TextInput source={"name"} validate={required()} />
    <ReferenceArrayInput source="person" reference="people" name={"person"}>
      <SelectArrayInput optionText="fullName" />
    </ReferenceArrayInput>
  </>
);
