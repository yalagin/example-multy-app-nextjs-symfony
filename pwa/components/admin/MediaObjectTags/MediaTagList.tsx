import {FieldGuesser, ListGuesser} from "@api-platform/admin";
import {ChipField, ReferenceArrayField, SingleFieldList, TextField} from "react-admin";
import AuthorField from "@/components/admin/customFields/AuthorField";
import AudioField from "@/components/admin/media/MediaField";

export  const MediaTagList = (props:any) => (
    <ListGuesser {...props}>
      <FieldGuesser source={"name"} />
      <ReferenceArrayField label="media" reference="media_objects" source="medaObject" >
        <SingleFieldList linkType="show">
          <AudioField  source={"contentUrl"} />
        </SingleFieldList>
      </ReferenceArrayField>
    </ListGuesser>
  );
