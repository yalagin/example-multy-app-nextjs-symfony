import {FieldGuesser, ShowGuesser} from "@api-platform/admin";
import {ChipField, ReferenceArrayField, SingleFieldList} from "react-admin";
import AudioField from "@/components/admin/media/MediaField";

const MediaTagShow = (props:any) => (
  <ShowGuesser {...props}>
    <FieldGuesser source={"@id"} />
    <FieldGuesser source={"name"} />
    <ReferenceArrayField label="media" reference="media_objects" source="medaObject" >
      <SingleFieldList linkType="show">
        <AudioField  source={"contentUrl"} />
      </SingleFieldList>
    </ReferenceArrayField>
  </ShowGuesser>
);
export default MediaTagShow;
