import {EditGuesser, EditGuesserProps} from "@api-platform/admin";
import {PersonTagsForm} from "@/components/admin/personTags/PersonTagsForm";


// @ts-ignore

export const PersonTagsEdit = (props: EditGuesserProps) => (
  // @ts-ignore
  <EditGuesser {...props} title="Edit Person Tags">
    <PersonTagsForm/>
  </EditGuesser>
);
