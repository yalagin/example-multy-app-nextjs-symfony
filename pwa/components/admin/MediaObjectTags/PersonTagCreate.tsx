import {CreateGuesser, type CreateGuesserProps, InputGuesser} from "@api-platform/admin";
import {AutocompleteInput, ReferenceArrayInput, ReferenceInput, SelectArrayInput, TextInput} from "react-admin";
import React from "react";
import {PersonTagsForm} from "@/components/admin/personTags/PersonTagsForm";

const PersonTagCreate = (props:CreateGuesserProps) => (
  <CreateGuesser {...props}>
   <PersonTagsForm/>
  </CreateGuesser>
);
export default PersonTagCreate;
