import { Menu as ReactAdminMenu } from "react-admin";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import VolumeUpIcon from '@mui/icons-material/VolumeUp';
import StyleIcon from '@mui/icons-material/Style';
import {AccessTimeFilled} from "@mui/icons-material";

const Menu = () => (
  <ReactAdminMenu>
    <ReactAdminMenu.Item to="/people" primaryText="Person" leftIcon={<MenuBookIcon/>}/>
    <ReactAdminMenu.Item to="/person_tags" primaryText="Person Tag" leftIcon={<StyleIcon/>}/>
    <ReactAdminMenu.Item to="/media_objects" primaryText="Recordings" leftIcon={<VolumeUpIcon/>}/>
    <ReactAdminMenu.Item to="/media_object_tags" primaryText="Recordings Tags" leftIcon={<StyleIcon/>}/>
    <ReactAdminMenu.Item to="/ages" primaryText="Age" leftIcon={<AccessTimeFilled/>}/>
  </ReactAdminMenu>
);
export default Menu;
