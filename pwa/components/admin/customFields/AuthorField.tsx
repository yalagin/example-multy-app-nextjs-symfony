import { useRecordContext, useGetOne } from 'react-admin';
// @ts-ignore
import { Link } from 'react-router-dom';

const AuthorField = () => {
  const post = useRecordContext();
  const { data, isLoading } = useGetOne('person', { id: post.person });

  return isLoading ? null : <Link to={post.person}>{data.name} {data.surname} </Link>;
};

export default AuthorField;
