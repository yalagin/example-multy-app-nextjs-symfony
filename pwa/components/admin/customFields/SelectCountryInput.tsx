import {useInput, UseInputValue} from 'react-admin';
import React, { useState, useMemo } from 'react'
import countryList from "react-select-country-list"
import { SelectField } from 'react-admin';
import Select from 'react-select'

function CountrySelector({ source, label }:any) {
  const { id, field, fieldState }: UseInputValue = useInput({ source });
  const [value, setValue] = useState('')
  const options = useMemo(() => countryList().getData(), [])

  const changeHandler = (value:any) => {
    field.onChange(value.value)
  }
  return (
    <label htmlFor={id}>
      {label}
      <Select options={options} id={id} {...field} onChange={changeHandler}/>
      {fieldState.error && <span>{fieldState.error.message}</span>}
    </label>
  );
  // return <Select options={options} value={value} onChange={changeHandler} />
}

export default CountrySelector

