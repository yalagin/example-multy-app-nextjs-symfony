import {Formik} from "formik";
import {type FunctionComponent} from "react";
import {type UseMutationResult} from "react-query";
import {
  Checkbox,
  debounce, FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  InputLabel, MenuItem,
  Radio,
  RadioGroup, Select,
  TextField,
  Typography
} from "@mui/material";

import {type FiltersProps} from "@/utils/book";
import {type FetchError, type FetchResponse} from "@/utils/dataAccess";
import {type PagedCollection} from "@/types/collection";
import {type Book} from "@/types/Book";

interface Props {
  filters: FiltersProps | undefined;
  mutation: UseMutationResult<FetchResponse<PagedCollection<Book>>>;
}

export const Filters: FunctionComponent<Props> = ({ filters, mutation }) => (
  <Formik
    initialValues={filters ?? {}}
    enableReinitialize={true}
    onSubmit={(values, { setSubmitting, setStatus, setErrors }) => {
      mutation.mutate(
        { ...values, page: 1 },
        {
          onSuccess: () => {
            setStatus({
              isValid: true,
            });
          },
          // @ts-ignore
          onError: (error: Error | FetchError) => {
            setStatus({
              isValid: false,
              msg: error.message,
            });
            if ("fields" in error) {
              setErrors(error.fields);
            }
          },
          onSettled: () => {
            setSubmitting(false);
          },
        }
      );
    }}
  >
    {({
      values,
      handleChange,
      handleSubmit,
      submitForm,
    }) => (
      <form onSubmit={handleSubmit}>

        <FormGroup className="mb-4">
          <FormControlLabel name="author" labelPlacement="top" className="!m-0" label={
            <Typography className="font-semibold w-full">Author</Typography>
          } control={
            <TextField value={values?.author ?? ""} placeholder="Search by author..." type="search"
                       data-testid="filter-author" variant="standard" className="w-full" onChange={(e) => {
                         handleChange(e);
                         debounce(submitForm, 1000)();
                       }}
            />
          }/>
        </FormGroup>
        <FormGroup className="mb-4">
          <FormControlLabel name="title" labelPlacement="top" className="!m-0" label={
            <Typography className="font-semibold w-full">Title</Typography>
          } control={
            <TextField value={values?.title ?? ""} placeholder="Search by title..." type="search"
                       data-testid="filter-title" variant="standard" className="w-full" onChange={(e) => {
                         handleChange(e);
                         debounce(submitForm, 1000)();
                       }}
            />
          }/>
        </FormGroup>
        <FormGroup className="mb-4">
          <FormControl>
            <Typography className="font-semibold w-full">Language</Typography>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              // value={language}
              label="Language"
              onChange={handleChange}
            >
              <MenuItem value={10}>English</MenuItem>
              <MenuItem value={20}>Thai</MenuItem>
              <MenuItem value={30}>German</MenuItem>
              <MenuItem value={30}>Chinese</MenuItem>
              <MenuItem value={30}>Russian</MenuItem>
            </Select>
          </FormControl>
        </FormGroup>

        <FormGroup>
          <ul className="block">
            <p className="font-semibold">Tones</p>
            <li>
              <FormControlLabel name="condition" label="Select All"
                                control={<Checkbox data-testid="filter-condition-new"/>}
                                checked={!!values?.condition?.includes("https://schema.org/NewCondition")}
                                value="https://schema.org/NewCondition"
                                onChange={(e) => {
                                  handleChange(e);
                                  submitForm();
                                }}
              />
            </li>
            <li>
              <FormControlLabel name="condition" label="Angry"
                                control={<Checkbox data-testid="filter-condition-damaged"/>}
                                checked={!!values?.condition?.includes("https://schema.org/DamagedCondition")}
                                value="https://schema.org/DamagedCondition"
                                onChange={(e) => {
                                  handleChange(e);
                                  submitForm();
                                }}
              />
            </li>
            <li>
              <FormControlLabel name="condition" label="Authoritative"
                                control={<Checkbox data-testid="filter-condition-refurbished"/>}
                                checked={!!values?.condition?.includes("https://schema.org/RefurbishedCondition")}
                                value="https://schema.org/RefurbishedCondition"
                                onChange={(e) => {
                                  handleChange(e);
                                  submitForm();
                                }}
              />
            </li>
            <li>
              <FormControlLabel name="condition" label="Inspiring"
                                control={<Checkbox data-testid="filter-condition-used"/>}
                                checked={!!values?.condition?.includes("https://schema.org/UsedCondition")}
                                value="https://schema.org/UsedCondition"
                                onChange={(e) => {
                                  handleChange(e);
                                  submitForm();
                                }}
              />
            </li>
            <li>
              <FormControlLabel name="condition" label="Playfull"
                                control={<Checkbox data-testid="filter-condition-used"/>}
                                checked={!!values?.condition?.includes("https://schema.org/UsedCondition")}
                                value="https://schema.org/UsedCondition"
                                onChange={(e) => {
                                  handleChange(e);
                                  submitForm();
                                }}
              />
            </li>
            <li>
              <FormControlLabel name="condition" label="Scared"
                                control={<Checkbox data-testid="filter-condition-used"/>}
                                checked={!!values?.condition?.includes("https://schema.org/UsedCondition")}
                                value="https://schema.org/UsedCondition"
                                onChange={(e) => {
                                  handleChange(e);
                                  submitForm();
                                }}
              />
            </li>
            <li>
              <FormControlLabel name="condition" label="Comedic"
                                control={<Checkbox data-testid="filter-condition-used"/>}
                                checked={!!values?.condition?.includes("https://schema.org/UsedCondition")}
                                value="https://schema.org/UsedCondition"
                                onChange={(e) => {
                                  handleChange(e);
                                  submitForm();
                                }}
              />
            </li>

            <li>
              <FormControlLabel name="condition" label="Sad"
                                control={<Checkbox data-testid="filter-condition-used"/>}
                                checked={!!values?.condition?.includes("https://schema.org/UsedCondition")}
                                value="https://schema.org/UsedCondition"
                                onChange={(e) => {
                                  handleChange(e);
                                  submitForm();
                                }}
              />
            </li>
          </ul>
        </FormGroup>
        <FormGroup className="mb-4">
          <FormControl>
            <Typography className="font-semibold w-full">Gender</Typography>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              defaultValue="female"
              name="radio-buttons-group"
            >
              <FormControlLabel value="female" control={<Radio/>} label="Female"/>
              <FormControlLabel value="male" control={<Radio/>} label="Male"/>
              <FormControlLabel value="other" control={<Radio />} label="Any" />
            </RadioGroup>
          </FormControl>
        </FormGroup>


        <FormGroup className="mb-4">
          <FormControl fullWidth>
            <Typography className="font-semibold w-full">Age (of the performer)</Typography>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              //value={age}
              label="Age"
              onChange={handleChange}
            >
              <MenuItem value={10}>Select all</MenuItem>
              <MenuItem value={20}>Senior</MenuItem>
              <MenuItem value={20}>Adult</MenuItem>
              <MenuItem value={30}>Young Adult</MenuItem>
              <MenuItem value={30}>Teen</MenuItem>
              <MenuItem value={30}>Child</MenuItem>
            </Select>
          </FormControl>
        </FormGroup>
      </form>
    )}
  </Formik>
);
